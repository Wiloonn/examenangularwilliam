import { Marque } from './marque';
import { TypeOrdi } from './type-ordi';
import  { Categorie } from "./categorie";

export class Ordinateur {

  id: number;
  model: string;
  marque: string;
  typeOrdi: string;
  categorie: string;
  prixAchat: number;
  prixVente: number;
  dateEntree: Date;


  constructor(id: number = null, model: string = null, marque: string = null, typeOrdi: string = null, categorie: string = null, prixAchat: number = null, prixVente: number = null, dateEntree: Date = new Date() ) {
    this.id = id;
    this.model = model;
    this.marque = marque;
    this.typeOrdi = typeOrdi;
    this.categorie = categorie;
    this.prixAchat = prixAchat;
    this.prixVente = prixVente;
    this.dateEntree = dateEntree;
  }
}

