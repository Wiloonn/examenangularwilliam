export class Marque {

  id: number;
  nomMarque: string;


  constructor(id: number = null, nomMarque: string = null) {
    this.id = id;
    this.nomMarque = nomMarque;
  }
}
