export class Categorie {

  id: number;
  nomCategorie: string;


  constructor(id: number, nomCategorie: string) {
    this.id = id;
    this.nomCategorie = nomCategorie;
  }
}
