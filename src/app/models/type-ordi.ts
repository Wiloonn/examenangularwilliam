export class TypeOrdi {

  id: number;
  nomTypeOrdi: string;


  constructor(id: number, nomTypeOrdi: string) {
    this.id = id;
    this.nomTypeOrdi = nomTypeOrdi;
  }
}
