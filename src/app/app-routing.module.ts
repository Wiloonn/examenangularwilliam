import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {OrdinateurComponent} from "./components/ordinateur/ordinateur.component";
import {AddOrdinateurComponent} from "./components/add-ordinateur/add-ordinateur.component";
import {OrdinateurDetailComponent} from "./components/ordinateur-detail/ordinateur-detail.component";
import {EditOrdinateurComponent} from "./components/edit-ordinateur/edit-ordinateur.component";


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'ordinateur', component: OrdinateurComponent},
  { path: 'ordinateur/add', component: AddOrdinateurComponent},
  { path: 'ordinateur/:id', component: OrdinateurDetailComponent},
  { path: 'edit-ordinateur/:id', component: EditOrdinateurComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
