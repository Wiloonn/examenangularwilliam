import { Component, OnInit } from '@angular/core';
import {Ordinateur} from "../../models/ordinateur";
import {ActivatedRoute, Router} from "@angular/router";
import {OrdinateurService} from "../../service/ordinateur.service";

@Component({
  selector: 'app-edit-ordinateur',
  templateUrl: './edit-ordinateur.component.html',
  styleUrls: ['./edit-ordinateur.component.css']
})
export class EditOrdinateurComponent implements OnInit {
  id: number;
  ordinateur: Ordinateur;

  constructor(private route: ActivatedRoute, private router: Router, private ordinateurService: OrdinateurService) { }

  ngOnInit(): void {
    this.ordinateurService.getOrdinateurByID(+this.route.snapshot.paramMap.get('id')).subscribe((data: Ordinateur) => {
      this.ordinateur = data;
    });
  }

  editOrdinateur() {
    //lance la fonction editChaussure de Chaussure.service
    this.ordinateurService.editOrdinateur(this.ordinateur).subscribe(then => {
      // change l'url avec la route '/Ordinateur'
      this.router.navigate(['/ordinateur']);
    });
  }
}
