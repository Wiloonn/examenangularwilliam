import { Component, OnInit } from '@angular/core';
import {OrdinateurService} from "../../service/ordinateur.service";
import {Ordinateur} from "../../models/ordinateur";

@Component({
  selector: 'app-ordinateur',
  templateUrl: './ordinateur.component.html',
  styleUrls: ['./ordinateur.component.css']
})
export class OrdinateurComponent implements OnInit {

  listeOrdinateurs: Ordinateur[];

  constructor(private ordinateurService: OrdinateurService) { }

  ngOnInit(): void {

    this.ordinateurService.getAllOrdinateurs().subscribe((data: Ordinateur[]) => {
      this.listeOrdinateurs = data;
    })
  }

  deleteOrdinateur(id: number) {
    const modelordinateur = this.listeOrdinateurs.find(ordinateur => ordinateur.id === id).model
    this.ordinateurService.deleteOrdinateur(id).subscribe(then => {
      this.ordinateurService.getAllOrdinateurs().subscribe((data: Ordinateur []) => {
        this.listeOrdinateurs = data;
      })
    });


  }

}
