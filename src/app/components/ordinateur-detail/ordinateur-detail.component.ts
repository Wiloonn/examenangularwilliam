import { Component, OnInit } from '@angular/core';
import {Ordinateur} from "../../models/ordinateur";
import {ActivatedRoute} from "@angular/router";
import {OrdinateurService} from "../../service/ordinateur.service";

@Component({
  selector: 'app-ordinateur-detail',
  templateUrl: './ordinateur-detail.component.html',
  styleUrls: ['./ordinateur-detail.component.css']
})
export class OrdinateurDetailComponent implements OnInit {
  ordinateur: Ordinateur;

  constructor(private route: ActivatedRoute, private ordinateurService: OrdinateurService) { }

  ngOnInit(): void {
    this.ordinateurService.getOrdinateurByID(+this.route.snapshot.paramMap.get('id')).subscribe((data: Ordinateur) => {
      this.ordinateur = data;
    });
  }

}
