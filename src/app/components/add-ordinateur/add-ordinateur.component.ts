import { Component, OnInit } from '@angular/core';
import {Ordinateur} from "../../models/ordinateur";
import {OrdinateurService} from "../../service/ordinateur.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-ordinateur',
  templateUrl: './add-ordinateur.component.html',
  styleUrls: ['./add-ordinateur.component.css']
})
export class AddOrdinateurComponent implements OnInit {
  ordinateur: Ordinateur;

  constructor(private ordinateurService: OrdinateurService, private router: Router) { }

  ngOnInit(): void {
    this.ordinateur = new Ordinateur();
  }

  submitOrdinateur() {
    this.ordinateurService.addOrdinateur(this.ordinateur).subscribe(then => {
      this.router.navigate(['/ordinateur'])
    })
  }
}
