import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Ordinateur} from "../models/ordinateur";
import {catchError, retry} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class OrdinateurService {
  apiUrl = 'http://localhost:3000/ordinateur';
    httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      })
    };

  constructor(private http: HttpClient) { }

  handleError(error) {
    let errorMessage = '';
    if ( error.error instanceof ErrorEvent ) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage)
  }

  getAllOrdinateurs() : Observable<Ordinateur[]> {
    return this.http.get<Ordinateur[]>(this.apiUrl).pipe(retry(1),catchError(this.handleError));
  }

  getOrdinateurByID (id: number): Observable <Ordinateur> {
    return this.http.get<Ordinateur>(this.apiUrl + '/' + id).pipe(retry(1), catchError(this.handleError));
  }

  addOrdinateur(Ordinateur: Ordinateur): Observable<Ordinateur> {
    return this.http.post<Ordinateur>(this.apiUrl ,Ordinateur).pipe(catchError(this.handleError));
  }

  editOrdinateur(Ordinateur: Ordinateur) {
    return this.http.put<Ordinateur>(this.apiUrl + '/' + Ordinateur.id, Ordinateur, this.httpOptions).pipe(catchError(this.handleError))
  }

  deleteOrdinateur(id: number): Observable <Ordinateur> {
    return this.http.delete<Ordinateur>(this.apiUrl + '/' + id).pipe(catchError(this.handleError))
  }
}
